package com.moviehome.springboot.api.services.implementation;

import com.moviehome.springboot.api.exceptions.ArgumentRequiredException;
import com.moviehome.springboot.api.exceptions.ConflictException;
import com.moviehome.springboot.api.exceptions.ModelNotFoundException;
import com.moviehome.springboot.api.models.Director;
import com.moviehome.springboot.api.repository.IDirectorRepo;
import com.moviehome.springboot.api.services.IDirectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class DirectorService implements IDirectorService {

    @Autowired
    private IDirectorRepo directorService;

    @Override
    public List<Director> getAll() {
        List<Director> directors = this.directorService.findAll();
        return directors;
    }

    @Override
    public Page<Director> getAll(Pageable pageable) {
        return (Page<Director>) this.directorService.findAll(pageable);
    }

    @Override
    public Director getById(Long id) throws ModelNotFoundException {
        Optional<Director> director = this.directorService.findById(id);
        if (director.isEmpty())
            throw new ModelNotFoundException("Director con id " + id + " no encontrado");
        return this.directorService.findById(id).get();
    }

    @Override
    public void save(Director newDirector) throws ConflictException {
        newDirector.setCreatedAt(new Date());
        if (this.directorService.existsByNamesIgnoreCase(newDirector.getNames())
            && this.directorService.existsByLastNamesIgnoreCase(newDirector.getLastNames())) {
            throw new ConflictException("Ya existe el director " + newDirector.getNames().concat(" " + newDirector.getLastNames()));
        }
        this.directorService.save(newDirector);
    }

    @Override
    public void edit(Director director) throws ModelNotFoundException, ConflictException, ArgumentRequiredException {

        if(director.getId() == null) {
            throw new ArgumentRequiredException("El id del director es obligatorio");
        }

        if (!this.directorService.existsById(director.getId())) {
            throw new ModelNotFoundException("No existe un director con id " + director.getId());
        }

        if (this.directorService.existsByNamesIgnoreCase(director.getNames())
                && this.directorService.existsByLastNamesIgnoreCase(director.getLastNames())) {
            throw new ConflictException("Ya existe el director " + director.getNames().concat(" " + director.getLastNames()));
        }
        director.setUpdatedAt(new Date());
        this.directorService.save(director);

    }

    @Override
    public void delete(Long id) throws ModelNotFoundException {
        if (!this.directorService.existsById(id))
            throw new ModelNotFoundException("No existe un director con id " + id);
        this.directorService.deleteById(id);
    }

}
