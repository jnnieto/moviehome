package com.moviehome.springboot.api.services.implementation;

import com.moviehome.springboot.api.models.Movie;
import com.moviehome.springboot.api.repository.IMovieRepo;
import com.moviehome.springboot.api.services.IMovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieService implements IMovieService {

    @Autowired
    private IMovieRepo movieRepo;

    @Override
    public List<Movie> getAll() {
        return this.movieRepo.findAll();
    }

    @Override
    public Page<Movie> getAll(Pageable pageable) {
        return null;
    }

    @Override
    public Movie getById(Long aLong) {
        return null;
    }

    @Override
    public void save(Movie movie) {
        this.movieRepo.save(movie);
    }

    @Override
    public void edit(Movie movie) {

    }

    @Override
    public void delete(Long aLong) {

    }

}
