package com.moviehome.springboot.api.services;

import com.moviehome.springboot.api.models.Director;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IDirectorService extends ICrud<Director, Long> {

    public Page<Director> getAll(Pageable pageable);

}
