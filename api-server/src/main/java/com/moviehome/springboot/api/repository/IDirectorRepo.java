package com.moviehome.springboot.api.repository;

import com.moviehome.springboot.api.models.Director;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDirectorRepo extends JpaRepository<Director, Long> {

    public Boolean existsByNamesIgnoreCase(String names);

    public Boolean existsByLastNamesIgnoreCase(String lastNames);

    public Boolean existsDirectorById(Long id);

}
