package com.moviehome.springboot.api.controllers;

import com.moviehome.springboot.api.exceptions.ConflictException;
import com.moviehome.springboot.api.exceptions.ModelNotFoundException;
import com.moviehome.springboot.api.models.Gender;
import com.moviehome.springboot.api.services.IGenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/genders")
public class GenderController {

    @Autowired
    private IGenderService genderService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<?> getAllGenders() {
        List<Gender> genders = this.genderService.getAll();
        if (genders.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Gender>>(genders, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<?> getGenderById(@PathVariable Long id) throws ModelNotFoundException {
        Gender gender = this.genderService.getById(id);
        return new ResponseEntity<Gender>(gender, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> saveNewGender(@Valid @RequestBody Gender newGender) throws ConflictException {
        this.genderService.save(newGender);
        return new ResponseEntity<Object>(HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> updateGender(
            @PathVariable Long id,
            @Valid @RequestBody Gender gender
    ) {
        List<Gender> genders = this.genderService.getAll();
        if (genders.isEmpty()) {
            return new ResponseEntity<>(genders, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(genders, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<?> deleteGenderById(@PathVariable Long id) throws ModelNotFoundException {
        this.genderService.delete(id);
        return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }

}
