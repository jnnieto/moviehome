package com.moviehome.springboot.api.services;

import com.moviehome.springboot.api.models.Gender;

public interface IGenderService extends ICrud<Gender, Long> {

}
