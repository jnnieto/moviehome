package com.moviehome.springboot.api.controllers;

import com.moviehome.springboot.api.exceptions.ConflictException;
import com.moviehome.springboot.api.models.Movie;
import com.moviehome.springboot.api.services.IMovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/movies")
public class MovieController {

    @Autowired
    private IMovieService movieService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<?> getAllMovies() {
        List<Movie> movies = this.movieService.getAll();
        if (movies.isEmpty()) {
            return new ResponseEntity<>(movies, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity saveMovie(@Valid @RequestBody Movie movie) throws ConflictException {
        this.movieService.save(movie);
        return new ResponseEntity<Object>(HttpStatus.CREATED);
    }

}
