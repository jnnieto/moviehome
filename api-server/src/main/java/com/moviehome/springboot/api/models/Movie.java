package com.moviehome.springboot.api.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "movies")
public class Movie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Title field is required")
    @Size(min = 2, max = 20, message = "Title must be between 2 and 20 characters")
    @Column(name = "title", length = 30)
    private String title;

    @NotNull(message = "Duration field is required")
    @Size(min = 2, max = 20, message = "Title must be between 2 and 20 characters")
    @Column(name = "duration", length = 10)
    private String duration;

    @NotNull(message = "Year field is required")
    @Min(value = 1900, message = "The movie must be launched since 1900")
    @Column(name = "year")
    private Integer year;

    @NotNull(message = "Synopsis field is required")
    @Size(min = 5, message = "Synopsis must be since 5 characters")
    @Column(name = "synopsis", columnDefinition = "text")
    private String synopsis;

    @Column(name = "image", columnDefinition = "text", nullable = true)
    private String image;

    @Column(name = "movie_file", length = 255)
    private String movieFile;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "updated_at")
    private Date updatedAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "director_id", nullable = false, foreignKey = @ForeignKey(name = "FK_Director_Movie"))
    private Director director;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gender_id", nullable = false, foreignKey = @ForeignKey(name = "FK_Gender_Movie"))
    private Gender gender;

    @PrePersist
    public void prePersist() {
        this.createdAt = new Date();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMovieFile() {
        return movieFile;
    }

    public void setMovieFile(String movieFile) {
        this.movieFile = movieFile;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

}
