package com.moviehome.springboot.api.controllers;

import com.moviehome.springboot.api.exceptions.ArgumentRequiredException;
import com.moviehome.springboot.api.exceptions.ConflictException;
import com.moviehome.springboot.api.exceptions.ModelNotFoundException;
import com.moviehome.springboot.api.models.Director;
import com.moviehome.springboot.api.services.IDirectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/directors")
public class DirectorController {

    @Autowired
    private IDirectorService directorService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<?> getAllDirectors() {
        List<Director> genders = this.directorService.getAll();
        if (genders.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Director>>(genders, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<?> getDirectorById(@PathVariable Long id) throws ModelNotFoundException {
        Director gender = this.directorService.getById(id);
        return new ResponseEntity<Director>(gender, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> saveNewDirector(@Valid @RequestBody Director newDirector) throws ConflictException {
        this.directorService.save(newDirector);
        return new ResponseEntity<Object>(HttpStatus.CREATED);
    }

    @PutMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> updateDirector(@Valid @RequestBody Director director) throws ConflictException, ArgumentRequiredException, ModelNotFoundException {
        this.directorService.edit(director);
        return new ResponseEntity<>(director, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<?> deleteDirectorById(@PathVariable Long id) throws ModelNotFoundException {
        this.directorService.delete(id);
        return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }

}
