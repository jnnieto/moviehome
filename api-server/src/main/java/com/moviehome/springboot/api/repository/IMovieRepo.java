package com.moviehome.springboot.api.repository;

import com.moviehome.springboot.api.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IMovieRepo extends JpaRepository<Movie, Long> {

}
