package com.moviehome.springboot.api.exceptions;

public class ModelNotFoundException extends Exception {

    public ModelNotFoundException(String message) {
        super(message);
    }

}
