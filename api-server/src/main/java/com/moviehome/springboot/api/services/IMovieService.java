package com.moviehome.springboot.api.services;

import com.moviehome.springboot.api.models.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IMovieService extends ICrud<Movie, Long> {

    public Page<Movie> getAll(Pageable pageable);

}
