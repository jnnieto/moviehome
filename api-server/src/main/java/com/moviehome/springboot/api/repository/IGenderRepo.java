package com.moviehome.springboot.api.repository;

import com.moviehome.springboot.api.models.Gender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IGenderRepo extends JpaRepository<Gender, Long> {

    public Optional<Gender> findByGenderName(String genderName);

    public Boolean existsGenderById(Long id);

}
