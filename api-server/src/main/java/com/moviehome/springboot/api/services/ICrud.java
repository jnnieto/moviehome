package com.moviehome.springboot.api.services;


import com.moviehome.springboot.api.exceptions.ArgumentRequiredException;
import com.moviehome.springboot.api.exceptions.ConflictException;
import com.moviehome.springboot.api.exceptions.ModelNotFoundException;

import java.util.List;

public interface ICrud<T, ID> {

    public List<T> getAll();

    public T getById(ID id) throws ModelNotFoundException;

    public void save(T object) throws ConflictException;

    public void edit(T object) throws ModelNotFoundException, ConflictException, ArgumentRequiredException;

    public void delete(ID id) throws ModelNotFoundException;

}
