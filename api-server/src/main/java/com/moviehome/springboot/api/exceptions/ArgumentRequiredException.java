package com.moviehome.springboot.api.exceptions;

public class ArgumentRequiredException extends Exception {

    public ArgumentRequiredException(String message) {
        super(message);
    }

}
