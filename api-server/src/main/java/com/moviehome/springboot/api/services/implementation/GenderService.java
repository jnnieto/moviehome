package com.moviehome.springboot.api.services.implementation;

import com.moviehome.springboot.api.exceptions.ArgumentRequiredException;
import com.moviehome.springboot.api.exceptions.ConflictException;
import com.moviehome.springboot.api.exceptions.ModelNotFoundException;
import com.moviehome.springboot.api.models.Gender;
import com.moviehome.springboot.api.repository.IGenderRepo;
import com.moviehome.springboot.api.services.IGenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GenderService implements IGenderService {

    @Autowired
    private IGenderRepo genderRepo;

    @Override
    public List<Gender> getAll() {
        List<Gender> genders = this.genderRepo.findAll();
        return genders;
    }

    @Override
    public Gender getById(Long id) throws ModelNotFoundException {
        Optional<Gender> gender = this.genderRepo.findById(id);
        if(gender.isEmpty()) {
            throw new ModelNotFoundException("No existe un género con id " + id);
        }
        return gender.get();
    }

    @Override
    public void save(Gender newGender) throws ConflictException {
        newGender.setGenderName(newGender.getGenderName().toLowerCase());
        Optional<Gender> gender = this.genderRepo.findByGenderName(newGender.getGenderName());
        if (gender.isPresent())
            throw new ConflictException("Ya existe un género con el nombre " + newGender.getGenderName());
        this.genderRepo.save(newGender);
    }

    @Override
    public void edit(Gender gender) throws ModelNotFoundException, ConflictException, ArgumentRequiredException {

    }

    @Override
    public void delete(Long id) throws ModelNotFoundException {
        if (!this.genderRepo.existsById(id))
            throw new ModelNotFoundException("No existe un género con id " + id);
        this.genderRepo.deleteById(id);
    }

}
